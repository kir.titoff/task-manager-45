package ru.t1.ktitov.tm.exception.field;

public final class EmptyProjectIdException extends AbstractFieldException {

    public EmptyProjectIdException() {
        super("Error! Project id is empty.");
    }

}
