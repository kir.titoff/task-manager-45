package ru.t1.ktitov.tm.api.service.dto;

import ru.t1.ktitov.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}
