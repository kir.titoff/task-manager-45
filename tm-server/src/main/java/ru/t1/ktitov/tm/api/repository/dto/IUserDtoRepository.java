package ru.t1.ktitov.tm.api.repository.dto;

import ru.t1.ktitov.tm.dto.model.UserDTO;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    UserDTO findByLogin(String login);

    UserDTO findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
