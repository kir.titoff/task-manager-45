package ru.t1.ktitov.tm.api.repository.model;

import ru.t1.ktitov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
