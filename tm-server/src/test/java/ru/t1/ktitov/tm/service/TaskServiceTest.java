/*
package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.ITaskService;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.service.dto.TaskService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService service = new TaskService(connectionService);

    private void compareTasks(@NotNull final TaskDTO task1, @NotNull final TaskDTO task2) {
        Assert.assertEquals(task1.getId(), task2.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getStatus(), task2.getStatus());
        Assert.assertEquals(task1.getUserId(), task2.getUserId());
        Assert.assertEquals(task1.getProjectId(), task2.getProjectId());
        Assert.assertEquals(task1.getCreated(), task2.getCreated());
    }

    private void compareTasks(
            @NotNull final List<TaskDTO> taskList1,
            @NotNull final List<TaskDTO> taskList2) {
        Assert.assertEquals(taskList1.size(), taskList2.size());
        for (int i = 0; i < taskList1.size(); i++) {
            compareTasks(taskList1.get(i), taskList2.get(i));
        }
    }

    @After
    public void tearDown() {
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        compareTasks(USER1_TASK1, service.findAll().get(0));
        compareTasks(USER2_TASK1, service.findAll().get(1));
    }

    @Test
    public void addByUserId() {
        service.add(USER1.getId(), USER1_TASK1);
        compareTasks(USER1_TASK1, service.findAll().get(0));
        Assert.assertEquals(USER1.getId(), service.findAll().get(0).getUserId());
    }

    @Test
    public void addList() {
        service.add(USER1_TASK_LIST);
        Assert.assertEquals(3, service.getSize());
        compareTasks(USER1_TASK1, service.findAll().get(0));
        compareTasks(USER1_TASK2, service.findAll().get(1));
        compareTasks(USER1_TASK3, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER1_TASK_LIST);
        service.set(USER2_TASK_LIST);
        compareTasks(USER2_TASK1, service.findAll().get(0));
    }

    @Test
    public void clear() {
        service.add(USER1_TASK_LIST);
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.clear(USER1.getId());
        Assert.assertEquals(1, service.getSize());
        compareTasks(USER2_TASK1, service.findAll().get(0));
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.add(ADMIN1_TASK_LIST);
        compareTasks(USER1_TASK_LIST, service.findAll(USER1.getId()));
        compareTasks(USER2_TASK_LIST, service.findAll(USER2.getId()));
        compareTasks(ADMIN1_TASK_LIST, service.findAll(ADMIN3.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        Assert.assertTrue(service.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(service.existsById(USER2_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1_TASK2.getId()));
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        compareTasks(USER1_TASK1, service.findOneById(USER1_TASK1.getId()));
        compareTasks(USER2_TASK1, service.findOneById(USER2_TASK1.getId()));
        compareTasks(USER1_TASK1, service.findOneById(USER1.getId(), USER1_TASK1.getId()));
        thrown.expect(EntityNotFoundException.class);
        compareTasks(USER1_TASK2, service.findOneById(USER1_TASK2.getId()));
        compareTasks(USER2_TASK1, service.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void remove() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1_TASK1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER1_TASK2.getId());
        Assert.assertEquals(2, service.getSize());
        compareTasks(USER1_TASK3, service.findAll().get(0));
        compareTasks(USER2_TASK1, service.findAll().get(1));
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeByUserId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER1.getId(), USER1_TASK2.getId());
        Assert.assertEquals(2, service.getSize());
        thrown.expect(EntityNotFoundException.class);
        service.remove(USER2.getId(), USER1_TASK3);
        Assert.assertEquals(2, service.getSize());
        compareTasks(USER1_TASK3, service.findAll().get(0));
        compareTasks(USER2_TASK1, service.findAll().get(1));
    }

    @Test
    public void create() {
        service.create(USER2.getId(), "task-2", "description of task 2");
        Assert.assertEquals(1, service.getSize());
        Assert.assertEquals("task-2", service.findAll().get(0).getName());
        Assert.assertEquals("description of task 2", service.findAll().get(0).getDescription());
        Assert.assertEquals(Status.NOT_STARTED, service.findAll().get(0).getStatus());
    }

    @Test
    public void findAllByProjectId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.add(ADMIN1_TASK_LIST);
        compareTasks(USER1_TASK_LIST, service.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final TaskDTO task = service.create(USER1.getId(),
                "task-4", "description of task 4");
        service.updateById(USER1.getId(), task.getId(),
                "upd-task-4", "upd description of task 4");
        @NotNull final TaskDTO foundTask = service.findOneById(task.getId());
        Assert.assertEquals("upd-task-4", foundTask.getName());
        Assert.assertEquals("upd description of task 4", foundTask.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        service.add(USER1_TASK1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        service.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        @NotNull final TaskDTO foundTask = service.findOneById(USER1_PROJECT1.getId());
        Assert.assertEquals(Status.IN_PROGRESS, foundTask.getStatus());
    }

}
*/
